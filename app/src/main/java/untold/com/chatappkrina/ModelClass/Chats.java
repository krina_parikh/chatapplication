package untold.com.chatappkrina.ModelClass;

import java.io.Serializable;
import java.util.Map;

public class Chats implements Serializable {


    // check user request
    public static final int FROM_USER = 0;
    public static final int FROM_FRIEND = 1;

    private int lastUpdateTime;

    public int getItem_type() {
        return item_type;
    }

    public void setItem_type(int item_type) {
        this.item_type = item_type;
    }

    public int item_type;

    private String from_user_color;
    private String from_user_id;
    private String from_user_name;
    private String id;

    private boolean isActive;


    private boolean isDeleted;
    private String subject;
    private String to_user_color;
    private String to_user_id;
    private String to_user_name;
    private int unreadCount;

    // user message details

    private Map<String,messages> message;
    // user typing details
    private IsTypingDTO isTyping;

    public Chats() {
    }

    public int getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(int lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }


    public String getFrom_user_color() {
        return from_user_color;
    }

    public void setFrom_user_color(String from_user_color) {
        this.from_user_color = from_user_color;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getFrom_user_name() {
        return from_user_name;
    }

    public void setFrom_user_name(String from_user_name) {
        this.from_user_name = from_user_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTo_user_color() {
        return to_user_color;
    }

    public void setTo_user_color(String to_user_color) {
        this.to_user_color = to_user_color;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getTo_user_name() {
        return to_user_name;
    }

    public void setTo_user_name(String to_user_name) {
        this.to_user_name = to_user_name;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public Map<String,messages> getMessageDetail() {
        return message;
    }

    public void setMessageDetail(Map<String,messages> messageDetail) {
        this.message = messageDetail;
    }

    public IsTypingDTO getIsTyping() {
        return isTyping;
    }

    public void setIsTyping(IsTypingDTO isTyping) {
        this.isTyping = isTyping;
    }


    @Override
    public String toString() {
        return "Chats{" +
                ", lastUpdateTime=" + lastUpdateTime +
                ", from_user_color='" + from_user_color + '\'' +
                ", from_user_id='" + from_user_id + '\'' +
                ", from_user_name='" + from_user_name + '\'' +
                ", id='" + id + '\'' +
                ", isActive=" + isActive +
                ", isDeleted=" + isDeleted +
                ", subject='" + subject + '\'' +
                ", to_user_color='" + to_user_color + '\'' +
                ", to_user_id='" + to_user_id + '\'' +
                ", to_user_name='" + to_user_name + '\'' +
                ", unreadCount=" + unreadCount +
                ", messageDetail=" + message +
                ", isTyping=" + isTyping +
                '}';
    }

}
