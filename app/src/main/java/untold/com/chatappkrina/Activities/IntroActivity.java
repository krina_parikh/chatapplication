package untold.com.chatappkrina.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import untold.com.chatappkrina.R;

public class IntroActivity extends AppCompatActivity {



    private TextView mTvWorld;
    private TextView mTvHello;
    private Button mBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);


        mTvWorld = (TextView) findViewById(R.id.mTvWorld);
        mTvHello = (TextView) findViewById(R.id.mTvHello);


        final ViewGroup transitionsContainer = (ViewGroup) findViewById(R.id.transitions_container);

        mTvWorld.setVisibility(View.INVISIBLE);
        mTvHello.setVisibility(View.INVISIBLE);


                mTvWorld.setVisibility(View.VISIBLE);
                Animation animSlide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_right_to_left_in);
                mTvWorld.startAnimation(animSlide);
                animSlide.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        mTvHello.setVisibility(View.VISIBLE);
//                        setAlphaAnimation(mTvHello);

                        Animation animSlide2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                        mTvHello.startAnimation(animSlide2);
                        animSlide2.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Intent intent = new Intent(IntroActivity.this, SplashScreenActivity.class);
                                startActivity(intent);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {


                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });


            }

}
