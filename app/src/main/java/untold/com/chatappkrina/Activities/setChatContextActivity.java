package untold.com.chatappkrina.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CpuUsageInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import untold.com.chatappkrina.Adapters.AllContactAdapters;
import untold.com.chatappkrina.Adapters.ChatContextAdapter;
import untold.com.chatappkrina.Adapters.ChatTextAdapter;
import untold.com.chatappkrina.Adapters.RequestListAdapter;
import untold.com.chatappkrina.Interface.onChatSubjectListener;
import untold.com.chatappkrina.ModelClass.Chats;
import untold.com.chatappkrina.ModelClass.ColorMap;
import untold.com.chatappkrina.ModelClass.ContactList;
import untold.com.chatappkrina.ModelClass.RequestDetails;
import untold.com.chatappkrina.ModelClass.Suggestions;
import untold.com.chatappkrina.ModelClass.UserDetails;
import untold.com.chatappkrina.R;
import untold.com.chatappkrina.Utils.KarlaTextView;

public class setChatContextActivity extends AppCompatActivity implements onChatSubjectListener {

    FloatingActionButton fb_sendRequest;
    EditText et_chatContext;
    RecyclerView rv_chatContextList;
    Toolbar toolbar_chatContext;
    LinearLayoutManager layoutManager;

    KarlaTextView tv_username;
    Context mContext;
    List<String> list_of_subject;


    ChatContextAdapter chatContextAdapter;

     Suggestions suggestions;
     ContactList contactList;

     String subject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_chat_context);

        mContext = this;


        suggestions = new Suggestions();
        list_of_subject = new ArrayList<>();


        contactList =  (ContactList) getIntent().getSerializableExtra("contactData");

        fb_sendRequest = (FloatingActionButton) findViewById(R.id.fb_sendRequest);
        et_chatContext = (EditText) findViewById(R.id.et_chatContext);
        rv_chatContextList =(RecyclerView) findViewById(R.id.rv_contextList);



        fb_sendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatabaseReference mDatabase;
                mDatabase = FirebaseDatabase.getInstance().getReference();
//                Intent i = new Intent(mContext,setChatContextActivity.class);
//                mContext.startActivity(i);
                ColorMap colorMap = new ColorMap();
                for (int j = 0; j < UserDetails.allUsers.size(); j++) {
                    if (UserDetails.allUsers.get(j).getCountact_number().equals(contactList.getContactNumber().replaceAll(" ", ""))) {
                        if (UserDetails.allUsers.get(j).getRequests()!=null){
                        ArrayList<RequestDetails> requestDetailsArrayList = new ArrayList<RequestDetails>(UserDetails.allUsers.get(j).getRequests().values());
                        for (int k = 0; k < requestDetailsArrayList.size(); k++) {
                            if (colorMap.getColorMap().get(requestDetailsArrayList.get(k).getFrom_user_name()) != null) {
                                colorMap.getColorMap().remove(requestDetailsArrayList.get(k).getFrom_user_name());
                            }
                        }}
                        final int random = new Random().nextInt(colorMap.getColorMap().size());
                        final String FromUserName = (new ArrayList<>(colorMap.getColorMap().keySet())).get(random);

                        RequestDetails requestDetails = new RequestDetails();
                        requestDetails.setDatetime(System.currentTimeMillis() + "");
                        requestDetails.setFrom_user_color(colorMap.getColorMap().get(FromUserName));
                        requestDetails.setFrom_user_id(UserDetails.userData.getId());
                        requestDetails.setId(UserDetails.userData.getId());
                        requestDetails.setFrom_user_name(FromUserName);
                        requestDetails.setSubject(subject); //TODO Show subject input for this
                        requestDetails.setTo_user_color("");
                        requestDetails.setTo_user_id(UserDetails.allUsers.get(j).getId());
                        requestDetails.setTo_user_name(contactList.getContactNumber());
                        mDatabase.child("Users").child(UserDetails.allUsers.get(j).getId())
                                .child("requests").child(UserDetails.userData.getId()).setValue(requestDetails)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(mContext, "Request sent success", Toast.LENGTH_LONG).show();
                                        // Write was successful!
                                        // ...

                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(mContext, "Request sent failed", Toast.LENGTH_LONG).show();
                                        // Write failed
                                        // ...
                                    }
                                });
                        return;
                    }
                }

                sendRequestViaMessage(contactList.getContactNumber());



            }
        });



        for (int i = 0;  i < 6; i ++)
        {

            list_of_subject.add("Hii Black Wants to talk with You");
            list_of_subject.add("Hii Grey Wants to talk with You");
            list_of_subject.add("Hii Brown Wants to talk with You");
            list_of_subject.add("Hii Yello Wants to talk with You");
            list_of_subject.add("Hii DOH Wants to talk with You");
            list_of_subject.add("Hii pINK Wants to talk with You");
            list_of_subject.add("Hii JHJH Wants to talk with You");

        }

        chatContextAdapter = new ChatContextAdapter(mContext,list_of_subject);
        layoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false);
        rv_chatContextList.setLayoutManager(layoutManager);
        rv_chatContextList.setAdapter(chatContextAdapter);






     //   rv_chatContextList.setAdapter(list_of_subject.get(chats_subject.get));

        //chats_subject.getSubject();


        }

    @Override
    public void OnChatSubjectClick(String text) {

        et_chatContext.setText(text);

        subject = et_chatContext.getText().toString();

    }

    private void sendRequestViaMessage(String number) {

        number = contactList.getContactNumber();
        number = number.replace(" ", "");

        new MessageSentAsyncTask(number).execute();
        }



    // sent message
     class MessageSentAsyncTask extends AsyncTask<String,String,String> {

        private String finalNumber;

        MessageSentAsyncTask(String number)
        {
           this.finalNumber = number;
        }
         @Override
         protected String doInBackground(String... strings) {


             //Your authentication key
             String authkey = "252372AOfpZSMEB5c18a8f7";
//Multiple mobiles numbers separated by comma

             //String mobiles =  number;


             String codeNumber;
             String codePlusSign = String.valueOf(finalNumber.charAt(0));

// canada code 1
             if(finalNumber.length() == 12) {
                 codeNumber = finalNumber.substring(1, 2);

             }
             else
             {
                // india code
                 codeNumber = finalNumber.substring(1, 2) + finalNumber.substring(2, 3);

             }


             String phoneNumber = finalNumber.substring(finalNumber.length() - 10);
             String countryCode = codePlusSign + codeNumber;

//Sender ID,While using route4 sender id should be 6 characters long.
             //String senderId = "102234";
//Your message to send, Add URL encoding here.
             String message = "Someone from your contact wants to discuss some important thing privately via Untold App Download it from";
//define route
             String route="1";

             URLConnection myURLConnection=null;
             URL myURL=null;
             BufferedReader reader=null;

//encoding message
             String encoded_message=  URLEncoder.encode(message);

//Send SMS API
             String mainUrl="http://api.msg91.com/api/sendhttp.php?";


//Prepare parameter string
             StringBuilder sbPostData= new StringBuilder(mainUrl);
             sbPostData.append("country="+codeNumber);
             sbPostData.append("&sender="+"Untold");
             sbPostData.append("&route="+route);
             sbPostData.append("&mobiles="+phoneNumber);
             sbPostData.append("&authkey="+authkey);
             sbPostData.append("&message="+encoded_message);

             // sbPostData.append("&sender="+senderId);

//final string
             mainUrl = sbPostData.toString();
             try
             {
                 //prepare connection
                 myURL = new URL(mainUrl);
                 try {
                     myURLConnection = myURL.openConnection();
                 } catch (IOException e) {
                     e.printStackTrace();
                 }
                 myURLConnection.connect();
                 reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));

                 //reading response
                 String response;
                 while ((response = reader.readLine()) != null)
                     //print response
                     Log.d("RESPONSE", ""+response);
                 //finally close connection
                 reader.close();
             }
             catch (IOException e)
             {
                 e.printStackTrace();
             }

             return null;
         }
         @Override
         protected void onPostExecute(String s) {
             super.onPostExecute(s);

             Toast.makeText(mContext,"Message Sent",Toast.LENGTH_LONG).show();
             Intent i = new Intent(setChatContextActivity.this,MainActivity.class);
             startActivity(i);
         }
     };
}
