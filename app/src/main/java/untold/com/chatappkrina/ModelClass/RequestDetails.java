package untold.com.chatappkrina.ModelClass;

public class RequestDetails {

    private String datetime;
    private String from_user_color;
    private String from_user_id;
    private String from_user_name;
    private String id;
    private boolean isDeleted;
    private String subject;
    private String to_user_color;
    private String to_user_id;
    private String to_user_name;

    public RequestDetails(){}

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getFrom_user_color() {
        return from_user_color;
    }

    public void setFrom_user_color(String from_user_color) {
        this.from_user_color = from_user_color;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getFrom_user_name() {
        return from_user_name;
    }

    public void setFrom_user_name(String from_user_name) {
        this.from_user_name = from_user_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTo_user_color() {
        return to_user_color;
    }

    public void setTo_user_color(String to_user_color) {
        this.to_user_color = to_user_color;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getTo_user_name() {
        return to_user_name;
    }

    public void setTo_user_name(String to_user_name) {
        this.to_user_name = to_user_name;
    }





}
