package untold.com.chatappkrina.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import untold.com.chatappkrina.R;


public class PrivacyFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_privacy, container, false);
        init(view);
        return view;
        }


    private void init(View view) {

        try {
            WebView wvTermsAndConditions = (WebView) view.findViewById(R.id.wvTermsAndConditions);
            wvTermsAndConditions.getSettings().setJavaScriptEnabled(true);
            wvTermsAndConditions.loadUrl("file:///android_asset/terms.html");

           /* ImageButton ibBackTerms = (ImageButton) view.findViewById(R.id.ibBackTerms);
            ibBackTerms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                    overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                }
            });*/
        } catch (Exception ex) {
            Log.e("init: ", ex + " >>");
        }

    }


}
