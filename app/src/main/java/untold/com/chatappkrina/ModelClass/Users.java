package untold.com.chatappkrina.ModelClass;

import java.util.List;
import java.util.Map;

public class Users {


    // FOR NUMBER
    private String countact_number;
    // user id
    private String id;

    // is user is online or not
    private  boolean isOnline;

    // user last seen
    private Long lastSeenTime;

    // Chat Details object
    private Map<String,Chats> chats;

    public Map<String, RequestDetails> getRequests() {
        return requests;
    }

    public void setRequests(Map<String, RequestDetails> requests) {
        this.requests = requests;
    }

    // user request details
    private Map<String,RequestDetails> requests;

    Map<String,DeviceDetails> deviceDetails;

    public Users(String countact_number, String id, boolean isOnline) {
        this.countact_number = countact_number;
        this.id = id;
        this.isOnline = isOnline;
    }

    public Map<String, Chats> getChats() {
        return chats;
    }

    public void setChats(Map<String, Chats> chats) {
        this.chats = chats;
    }

    public Map<String, DeviceDetails> getDeviceDetails() {
        return deviceDetails;
    }

    public void setDeviceDetails(Map<String, DeviceDetails> deviceDetails) {
        this.deviceDetails = deviceDetails;
    }

    public Users() {
    }

    public  String getCountact_number() {
        return countact_number;
    }

    public  void setCountact_number(String countact_number) {
        this.countact_number = countact_number;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public Long getLastSeenTime() {
        return lastSeenTime;
    }

    public void setLastSeenTime(Long lastSeenTime) {
        this.lastSeenTime = lastSeenTime;
    }






}
