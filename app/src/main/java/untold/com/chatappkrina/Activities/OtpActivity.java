package untold.com.chatappkrina.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import untold.com.chatappkrina.Interface.ISmsListener;
import untold.com.chatappkrina.ModelClass.DeviceDetails;
import untold.com.chatappkrina.ModelClass.UserDetails;
import untold.com.chatappkrina.ModelClass.Users;
import untold.com.chatappkrina.R;
import untold.com.chatappkrina.Recievers.IncomingSMS;
import untold.com.chatappkrina.Utils.SharedPreferenceHelper;

public class OtpActivity extends AppCompatActivity implements ISmsListener {


    TextView tv_getotp, tv_verify, tv_set_number;
    EditText et_otp;
    EditText tv_number;
    TextInputLayout edtxt_otp;
    String otpCode;
    CardView cv_getnumber, cv_verifyotp;
    String number;
    private String verificationId;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        IncomingSMS.bind(this);
        mAuth = FirebaseAuth.getInstance();
        Firebase.setAndroidContext(this);
        cv_getnumber = (CardView) findViewById(R.id.card_view_number);
        cv_verifyotp = (CardView) findViewById(R.id.card_view_otp);
        tv_number = findViewById(R.id.tv_number);
        tv_set_number = (TextView) findViewById(R.id.tv_set_number);
        tv_verify = (TextView) findViewById(R.id.tv_verify);
        tv_getotp = (TextView) findViewById(R.id.tv_getotp);
        et_otp = (EditText) findViewById(R.id.et_otp);
        //otpView = (OtpView) findViewById(R.id.otp);
        edtxt_otp =(TextInputLayout)findViewById(R.id.edtxt_otp);


        InputMethodManager ipmm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        ipmm.hideSoftInputFromWindow(et_otp.getWindowToken(), 0);


        tv_getotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                number = tv_number.getText().toString();

                if (number.length() == 0 || number.equals("") || number == null) {
                    Toast.makeText(OtpActivity.this, "Please Enter Your phone number", Toast.LENGTH_SHORT).show();
                } else {
                    cv_getnumber.setVisibility(View.INVISIBLE);
                    cv_verifyotp.setVisibility(View.VISIBLE);
                    tv_set_number.setText(number);
                    sendVerificationCode(number);

                    edtxt_otp.setVisibility(View.VISIBLE);
                    tv_getotp.setText("Verify");
                }

            }
        });


        tv_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String code = tv_verify.getText().toString().trim();
                    if (code.isEmpty() || code.length() < 6) {
                        tv_verify.setError("Enter code...");
                        tv_verify.requestFocus();
                        return;
                    }
                    Intent intent = new Intent(OtpActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
//                    verifyCode(code);
                }catch (Exception e){
                    e.printStackTrace();
                }


            }
        });


    }

    private void verifyCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithCredential(credential);
    }

    private void signInWithCredential(final PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            UserDetails.contactNo = number;
                            addToFBDB();
                            Intent intent = new Intent(OtpActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        } else {
                            Toast.makeText(OtpActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void addToFBDB() {
        String url = "https://chatapptestandroid.firebaseio.com/Users.json";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Firebase reference = new Firebase("https://chatapptestandroid.firebaseio.com/users");

                if (s.equals("null")) {
                    createNewUser(null);
                } else {
                    try {
                        JSONObject obj = new JSONObject(s);

                        if (!obj.toString().contains(number)) {
                            createNewUser(null);
                        } else {
                            createNewUser(null); //TODO pass user iD here
                            Toast.makeText(OtpActivity.this, "username already exists", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError);
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(OtpActivity.this);
        rQueue.add(request);


//https://chatapptestandroid.firebaseio.com/
//                                String url = "https://androidchatapp-76776.firebaseio.com/users.json";


    }

    private void createNewUser(String s) {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();

        DatabaseReference usersRef = ref.child("Users");
        String mContactId = s == null ? usersRef.push().getKey() : s;
//        Map<String, Users> users = new HashMap<>();
//        users.put(mContactId, new Users(number, mContactId, true));
        Users usr = new Users(number, mContactId, true);
        DatabaseReference deviceRef = usersRef.child(mContactId).child("devices");
        String deviceKey = deviceRef.push().getKey();

        DeviceDetails details = new DeviceDetails();
        details.setId(SharedPreferenceHelper.getSharedPreferenceString(this, "fb_token", ""));
        details.setType("a");
        Map<String, DeviceDetails> deviceDetailsMap = new HashMap<String, DeviceDetails>();
        deviceDetailsMap.put(deviceKey, details);
        usr.setDeviceDetails(deviceDetailsMap);
        usersRef.child(mContactId).setValue(usr);

        SharedPreferenceHelper.setSharedPreferenceString(this, "user_id", mContactId);
    }

    private void sendVerificationCode(String number) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallBack
        );

    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId = s;
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                et_otp.setText(code);
                verifyCode(code);
            } else {
                UserDetails.contactNo = number;
                addToFBDB();
                Intent intent = new Intent(OtpActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);

            }

        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(OtpActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    };

    @Override
    public void smsReceived(String messageText) {
/*
        // OTP RECIVER
        et_otp.setText(messageText);

        */
    }
}
