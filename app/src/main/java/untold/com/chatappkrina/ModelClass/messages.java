package untold.com.chatappkrina.ModelClass;

import java.io.Serializable;

public class messages implements Serializable {


    private int datetime;
    private String id;
    private boolean isRead;
    private int readTime;
    private String sender_id;
    private String text;

    public void setDatetime(int datetime){
        this.datetime = datetime;
    }

    public int getDatetime(){
        return datetime;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setIsRead(boolean isRead){
        this.isRead = isRead;
    }

    public boolean isIsRead(){
        return isRead;
    }

    public void setReadTime(int readTime){
        this.readTime = readTime;
    }

    public int getReadTime(){
        return readTime;
    }

    public void setSenderId(String senderId){
        this.sender_id = senderId;
    }

    public String getSenderId(){
        return sender_id;
    }

    public void setText(String text){
        this.text = text;
    }

    public String getText(){
        return text;
    }

    @Override
    public String toString(){
        return
                "LTJ7xPpbh0baLFgsODTO{" +
                        "datetime = '" + datetime + '\'' +
                        ",id = '" + id + '\'' +
                        ",isRead = '" + isRead + '\'' +
                        ",readTime = '" + readTime + '\'' +
                        ",sender_id = '" + sender_id+ '\'' +
                        ",text = '" + text + '\'' +
                        "}";
    }
}
