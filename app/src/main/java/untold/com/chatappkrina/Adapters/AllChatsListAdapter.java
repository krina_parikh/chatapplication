package untold.com.chatappkrina.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import untold.com.chatappkrina.Activities.ChatActivity;
import untold.com.chatappkrina.ModelClass.Chats;
import untold.com.chatappkrina.ModelClass.Users;
import untold.com.chatappkrina.R;

public class AllChatsListAdapter extends RecyclerView.Adapter<AllChatsListAdapter.ChatListHolder>{


    private List<Chats> chatDisplayList;
    private Context mContext;
    public AllChatsListAdapter(List<Chats> chatList, Context mContext){

        this.chatDisplayList = chatList;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public AllChatsListAdapter.ChatListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_allchats, null);
        AllChatsListAdapter.ChatListHolder chatListViewHolder = new AllChatsListAdapter.ChatListHolder(view);
        return chatListViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AllChatsListAdapter.ChatListHolder chatListHolder, int i) {

        Chats userDetails = chatDisplayList.get(i);

        chatListHolder.tvFriendName.setText(userDetails.getFrom_user_name());
        chatListHolder.tvChatTime.setText(userDetails.getLastUpdateTime());
        chatListHolder.tvChatDetail.setText(userDetails.getSubject());

        chatListHolder.rl_chatDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, ChatActivity.class);
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return chatDisplayList.size();
    }


    public static class ChatListHolder extends RecyclerView.ViewHolder{


        RelativeLayout rl_chatDetails;
        TextView tvFriendName;
        TextView tvChatTime;
        TextView tvChatDetail;


        public ChatListHolder(View itemView) {
            super(itemView);

            rl_chatDetails = (RelativeLayout) itemView.findViewById(R.id.rl_chatDetials);
            tvFriendName = (TextView) itemView.findViewById(R.id.tv_friendName);
            tvChatTime = (TextView) itemView.findViewById(R.id.tv_chatTime);
            tvChatDetail = (TextView)  itemView.findViewById(R.id.tv_lastChatMessage);
        }
    }




}
