package untold.com.chatappkrina.ModelClass;

public class DeviceDetails {
    public DeviceDetails() {
    }
    String id;
    String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
