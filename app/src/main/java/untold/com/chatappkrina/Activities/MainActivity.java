package untold.com.chatappkrina.Activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import untold.com.chatappkrina.Fragments.BlockedUserFragment;
import untold.com.chatappkrina.Fragments.ChatListFragment;
import untold.com.chatappkrina.Fragments.FeedBackFragment;
import untold.com.chatappkrina.Fragments.LogoutFragment;
import untold.com.chatappkrina.Fragments.PrivacyFragment;
import untold.com.chatappkrina.Fragments.ShareFragment;
import untold.com.chatappkrina.Fragments.TermsFragment;
import untold.com.chatappkrina.ModelClass.Chats;
import untold.com.chatappkrina.ModelClass.UserDetails;
import untold.com.chatappkrina.ModelClass.Users;
import untold.com.chatappkrina.R;
import untold.com.chatappkrina.Utils.SharedPreferenceHelper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    Context mContext;
    DrawerLayout drawer;
    Toolbar side_toolbar;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.mipmap.nav_drawer);

        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {

                    drawer.closeDrawer(GravityCompat.START);
                    toggle.setHomeAsUpIndicator(R.mipmap.icon_back);
                } else {
                    toggle.setHomeAsUpIndicator(R.mipmap.nav_drawer);
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        displaySelectedScreen(R.id.nav_chat);

        mContext = this;
        getAllUserData();



        // notification code

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }



    }

    private void getAllUserData() {


      //String url = "https://chatapptestandroid.firebaseio.com/Users.json";
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference users = rootRef.child("Users");
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    UserDetails.allUsers.add(ds.getValue(Users.class));
                   if (ds.getKey().equals(SharedPreferenceHelper.getSharedPreferenceString(MainActivity.this,"user_id",""))){
                       Users users  = ds.getValue(Users.class);
                       Log.d("log_tag", users.getId());
                       UserDetails.userData = users;

                       if (fragment instanceof ChatListFragment){
                           ((ChatListFragment)fragment).data();
                       }
                   }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        users.addListenerForSingleValueEvent(eventListener);

    }

   /* @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
*/
    @Override
    public void onBackPressed() {

        FragmentManager fragments = getSupportFragmentManager();

        if (fragments.getBackStackEntryCount() > 1) {
            //  fragments.popBackStack(null,fragments.);

            if(!fragments.popBackStackImmediate())
            {
                finish();
            }

        }

        else
        {
            // supportFinishAfterTransition();
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();


            }
        }
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
*/

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        displaySelectedScreen(menuItem.getItemId());

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }
    Fragment fragment = null;

    private void displaySelectedScreen(int itemId) {

        //creating fragment object
        FragmentManager fragments = getSupportFragmentManager();
        FragmentTransaction ft = fragments.beginTransaction();
        fragments.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        ft.commit();

        //initializing the fragment object which is selected
        switch (itemId) {

            case R.id.nav_share:
                fragment = new ShareFragment();
                break;

            case R.id.nav_chat:
                fragment = new ChatListFragment();
                break;

            case R.id.nav_blockUser:
                fragment = new BlockedUserFragment();
                break;

            case R.id.nav_feedback:
                fragment = new FeedBackFragment();
                break;

            case R.id.nav_privacy:
                fragment = new PrivacyFragment();
                break;

            case R.id.nav_tc:
                fragment = new TermsFragment();
                break;

            case R.id.nav_logout:
                fragment = new LogoutFragment();
                break;

        }

        if (fragment != null) {

            final FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction();

            // put the fragment in place
            transaction.replace(R.id.content_frame, fragment);

            // this is the part that will cause a fragment to be added to backstack,
            // this way we can return to it at any time using this tag
            transaction.commit();
        }

    }

}
