package untold.com.chatappkrina.Activities;

import android.content.Intent;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import untold.com.chatappkrina.R;
import untold.com.chatappkrina.Utils.SharedPreferenceHelper;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);



        if (SharedPreferenceHelper.getSharedPreferenceString(this,"user_id",null)==null){
            TaskStackBuilder.create(this)
                    .addNextIntentWithParentStack(new Intent(this, OtpActivity.class))
                    .addNextIntent(new Intent(this, WalkThroughActivity.class))
                    .startActivities();
        }else {
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }
    }
}
