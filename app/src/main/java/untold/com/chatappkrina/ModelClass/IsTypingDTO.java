package untold.com.chatappkrina.ModelClass;


import java.io.Serializable;

public class IsTypingDTO implements Serializable {

	private boolean first_user;
	private boolean second_user;

	public void setFirstUser(boolean firstUser){
		this.first_user = firstUser;
	}

	public boolean isFirstUser(){
		return first_user;
	}

	public void setSecondUser(boolean secondUser){
		this.second_user = secondUser;
	}

	public boolean isSecondUser(){
		return second_user;
	}

	@Override
 	public String toString(){
		return 
			"IsTypingDTO{" + 
			"first_user = '" + first_user + '\'' +
			",second_user = '" + second_user + '\'' +
			"}";
		}
}