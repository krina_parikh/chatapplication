package untold.com.chatappkrina.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

import untold.com.chatappkrina.ModelClass.ContactList;

public class DatabaseHelper extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Untold";
    private static final String TABLE_NAME = "PhoneContact";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_NUMBER = "number";

    private static final String[] COLUMNS = {KEY_ID, KEY_NAME, KEY_NUMBER,
    };

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATION_TABLE = "CREATE TABLE PhoneContact ( "
                + "id INTEGER PRIMARY KEY AUTOINCREMENT, " + "name TEXT, "
                + "number TEXT)";

        db.execSQL(CREATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // you can implement here migration process
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        this.onCreate(db);
    }

    public void deleteDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
    }

    // get only one contact

    ContactList getcontact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{KEY_ID, KEY_NAME, KEY_NUMBER}, KEY_ID + "-?", new String[]{

        }, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        ContactList contactList = new ContactList(Integer.parseInt
                (cursor.getString(0)),
                cursor.getString(1), cursor.getString(2),
                cursor.getString(3));

        return contactList;

    }


    public ContactList getContactList(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, // a. table
                COLUMNS, // b. column names
                " id = ?", // c. selections
                new String[]{String.valueOf(id)}, // d. selections args
                null, // e. group by
                null, // f. having
                null, // g. order by
                null); // h. limit

        if (cursor != null)
            cursor.moveToFirst();

        ContactList contactList = new ContactList();
        contactList.setContactId(Integer.parseInt(cursor.getString(0)));
        contactList.setContactName(cursor.getString(1));
        contactList.setContactNumber(cursor.getString(2));


        return contactList;
    }

    public List<ContactList> allContacts() {

        List<ContactList> contactLists = new LinkedList<ContactList>();
        String query = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        ContactList contct = null;

        if (cursor.moveToFirst()) {
            do {
                contct = new ContactList();
                contct.setContactId(Integer.parseInt(cursor.getString(0)));
                contct.setContactName(cursor.getString(1));
                contct.setContactNumber(cursor.getString(2));

                contactLists.add(contct);

            } while (cursor.moveToNext());
        }

        return contactLists;
    }

    public void addPlayer(ContactList player) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, player.getContactName());
        values.put(KEY_NUMBER, player.getContactNumber());

        // insert
        db.insert(TABLE_NAME, null, values);
        db.close();
    }


    private int getID(String name, String job) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.query(TABLE_NAME, new String[]{"*"}, "name =? AND number=?",
                new String[]{name, job}, null, null, null, null);
        if (c.moveToFirst()) //if the row exist then return the id
            return c.getInt(c.getColumnIndex("id"));
        return -1;
    }


    public void updateContactList(ContactList contact) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getContactName());
        values.put(KEY_NUMBER, contact.getContactNumber());

        int id = getID(contact.getContactName(), contact.getContactNumber());
        if (id == -1)
            db.insert(TABLE_NAME, null, values);
        else
            db.update(TABLE_NAME, values, "id=?", new String[]{Integer.toString(id)});


//        int i = db.update(TABLE_NAME, // table
//                values, // column/value
//                "id = ?", // selections
//                new String[] { String.valueOf(contactList.getContactId())});

        db.close();

    }

}



