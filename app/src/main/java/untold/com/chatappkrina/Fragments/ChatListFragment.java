package untold.com.chatappkrina.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import untold.com.chatappkrina.Activities.ContactListActivity;
import untold.com.chatappkrina.Adapters.AllChatsListAdapter;
import untold.com.chatappkrina.Adapters.AllContactAdapters;
import untold.com.chatappkrina.Adapters.RequestListAdapter;
import untold.com.chatappkrina.Interface.OnDataChange;
import untold.com.chatappkrina.ModelClass.Chats;
import untold.com.chatappkrina.ModelClass.RequestDetails;
import untold.com.chatappkrina.ModelClass.UserDetails;
import untold.com.chatappkrina.R;
import untold.com.chatappkrina.Utils.KarlaTextView;


public class ChatListFragment extends Fragment implements OnDataChange {


    private InterstitialAd mInterstitialAd;

    private AllChatsListAdapter allChatsListAdapter;

    private RequestListAdapter requestListAdapter;
    RecyclerView rv_allchatList, rv_requestList;
    private AllContactAdapters mAdapter;
    private LinearLayoutManager layoutManager;
    List<Chats> user_details = new ArrayList();
    List<RequestDetails> requestDetailsList = new ArrayList<>();
    Chats objChatDetails;
    RequestDetails requestDetails;
    Context mContext;
    FloatingActionButton fb_fetchConatct;
    CardView cv_request;

    KarlaTextView tv_noRequest;

    private static final long GAME_LENGTH_MILLISECONDS = 3000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        mContext = getActivity();

        cv_request = (CardView) view.findViewById(R.id.cv_requestRecycler);
        rv_requestList = (RecyclerView) view.findViewById(R.id.rv_requestList);
        rv_allchatList = (RecyclerView) view.findViewById(R.id.rv_chatList);
        layoutManager = new LinearLayoutManager(mContext);
        rv_allchatList.setLayoutManager(layoutManager);
        allChatsListAdapter = new AllChatsListAdapter(user_details, mContext);
        rv_allchatList.setAdapter(allChatsListAdapter);

        tv_noRequest = (KarlaTextView) view.findViewById(R.id.tv_noRequest);
        fb_fetchConatct = (FloatingActionButton) view.findViewById(R.id.fb_fetchContact);

        MobileAds.initialize(mContext,
                "ca-app-pub-9085854059723868~3003834452");
        mInterstitialAd = new InterstitialAd(mContext);
        mInterstitialAd.setAdUnitId("ca-app-pub-9085854059723868/4568321279");

        listeners();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                startGame();
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the interstitial ad is closed.
            }
        });

//        getChatDetails();
//        getRequestDetails();

        return view;

    }

    private void startGame() {

        }

    // listeners
    private void listeners() {

        fb_fetchConatct.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getActivity(), ContactListActivity.class);
                        startActivity(i);
                        getActivity().overridePendingTransition(R.anim.slide_up_info, R.anim.no_change);

                    }
                });


    }

    //get chat details
    private void getChatDetails() {

        String url = "https://chatapptestandroid.firebaseio.com/Chats.json";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                Log.d("log_tag", s);

                try {

                    JSONObject reqJsonObject = new JSONObject(s);
                    for (int i = 0; i < reqJsonObject.length(); i++) {

                        Chats details = new Chats();
                        details.getFrom_user_color();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError);
            }
        });
        if (getActivity() != null) {
            RequestQueue rQueue = Volley.newRequestQueue(getActivity());
            rQueue.add(request);
        }

    }

    @Override
    public void data() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (UserDetails.userData.getRequests() != null) {


                    requestDetailsList = new ArrayList<RequestDetails>(UserDetails.userData.getRequests().values());
                    requestListAdapter = new RequestListAdapter(requestDetailsList, mContext);
                    layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                    rv_requestList.setLayoutManager(layoutManager);
                    rv_requestList.setAdapter(requestListAdapter);

                    tv_noRequest.setVisibility(View.INVISIBLE);
                    cv_request.setVisibility(View.VISIBLE);
                }

                if (UserDetails.userData.getChats() != null) {
                    user_details = new ArrayList<Chats>(UserDetails.userData.getChats().values());
                    allChatsListAdapter = new AllChatsListAdapter(user_details, mContext);
                    rv_allchatList.setAdapter(allChatsListAdapter);
                }
            }
        });

    }
}
