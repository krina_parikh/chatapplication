package untold.com.chatappkrina.Recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;



import untold.com.chatappkrina.Interface.ISmsListener;


public class IncomingSMS extends BroadcastReceiver {

    private static ISmsListener smsListener;
    public static String VERIFICATION_CODE;
    //final SmsManager sms = SmsManager.getDefault();

    @Override
    public void onReceive(Context context, Intent receiverIntent) {

        final Bundle bundle = receiverIntent.getExtras();

        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    /*String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String senderNum = phoneNumber ;*/
                    String message = currentMessage.getDisplayMessageBody();

                    String lastFourDigits = message.substring(message.length() - 4);
                    VERIFICATION_CODE = lastFourDigits;
                    if (smsListener != null) {
                        smsListener.smsReceived(lastFourDigits);
                    }
                }
            }
        } catch (Exception e) {
        }

    }

    public static void bind(ISmsListener listener) {
        smsListener = listener;
    }
}
