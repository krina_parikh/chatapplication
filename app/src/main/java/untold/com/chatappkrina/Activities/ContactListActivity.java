package untold.com.chatappkrina.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import untold.com.chatappkrina.Adapters.AllContactAdapters;
import untold.com.chatappkrina.Database.DatabaseHelper;
import untold.com.chatappkrina.Interface.onContactListener;
import untold.com.chatappkrina.ModelClass.ContactList;
import untold.com.chatappkrina.R;

public class ContactListActivity extends AppCompatActivity implements onContactListener {


    Context mContext;
    Toolbar toolbar;
    ProgressBar progressBar;
    RecyclerView rv_contact;
    EditText et_searchConatct;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    Button btnRef;
    FloatingActionButton fb_sendConatct;
    private AllContactAdapters mAdapter;
    ImageButton ib_closeRequest;
    private LinearLayoutManager layoutManager;


    List<ContactList> contactVOList = new ArrayList();
    ContactList contactVO;

    ContactList getSelecedObject;


    private DatabaseHelper mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        btnRef = findViewById(R.id.btnRef);

        mContext = this;
        mydb = new DatabaseHelper(mContext);
        //  getUpdatedAppUsers();
        et_searchConatct = (EditText) findViewById(R.id.et_searchContactView);
        rv_contact = (RecyclerView) findViewById(R.id.rvContacts);
        progressBar = (ProgressBar) findViewById(R.id.p_bar);
        fb_sendConatct = (FloatingActionButton) findViewById(R.id.fb_selectContact);

        ib_closeRequest = (ImageButton) findViewById(R.id.ib_closeRequest);
        btnRef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!updateTask.getStatus().equals(AsyncTask.Status.RUNNING))
                updateTask.execute();
            }
        });
        ib_closeRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();

            }
        });

        fb_sendConatct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, setChatContextActivity.class);
                i.putExtra("contactData", getSelecedObject);
                mContext.startActivity(i);
            }
        });

        // Read and show the contacts

        searchConatct();
        showContacts();
    }

    private void getUpdatedAppUsers() {


    }

    private void searchConatct() {

        et_searchConatct.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = et_searchConatct.getText().toString().toLowerCase(Locale.getDefault());
                mAdapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void showContacts() {
        //  permission check
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        } else {
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {

                    progressBar.setVisibility(View.VISIBLE);
                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... voids) {

                    progressBar.setVisibility(View.VISIBLE);
                    try {

                        if (mydb.allContacts().size() == 0) {
                            contactVOList = getAllContacts();
                        } else {
                            contactVOList = mydb.allContacts();
                            //  updateContactList();
                        }

                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    progressBar.setVisibility(View.GONE);
                    layoutManager = new LinearLayoutManager(mContext);
                    rv_contact.setLayoutManager(layoutManager);
                    mAdapter = new AllContactAdapters(contactVOList, mContext);
                    rv_contact.setAdapter(mAdapter);
                }
            }.execute();


        }
    }

    AsyncTask updateTask = new AsyncTask<Object, Void, Void>() {
        @Override
        protected Void doInBackground(Object... voids) {
            try {
                mydb.deleteDB();
                getAllContacts();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(mContext, "Contacts Updated", Toast.LENGTH_SHORT).show();
            contactVOList = mydb.allContacts();
            layoutManager = new LinearLayoutManager(mContext);
            rv_contact.setLayoutManager(layoutManager);
            mAdapter = new AllContactAdapters(contactVOList, mContext);
            rv_contact.setAdapter(mAdapter);
        }
    };


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    private ArrayList<ContactList> getAllContacts() throws RemoteException {
        ArrayList<ContactList> arrayListContact = new ArrayList<>();
        ContentResolver contentResolver = mContext.getContentResolver();
        // ContentProviderClient mCProviderClient = contentResolver.acquireContentProviderClient(ContactsContract.Contacts.CONTENT_URI);

        Cursor mCursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null,
                null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

      /*  Cursor mCursor = mCProviderClient.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
*/

        if (mCursor.getCount() > 0) {
            while (mCursor.moveToNext()) {

                int hasPhoneNumber = Integer.parseInt(mCursor.getString(mCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    String id = mCursor.getString(mCursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = mCursor.getString(mCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    contactVO = new ContactList();
                    contactVO.setContactName(name);


                    Cursor phoneCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id},
                            null);
                    if (phoneCursor.moveToNext()) {
                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contactVO.setContactNumber(phoneNumber);
                    }


                    phoneCursor.close();


                    mydb.addPlayer(contactVO);
                    arrayListContact.add(contactVO);
                }
            }


        }
        return arrayListContact;
    }

    @Override
    public void contactClick(ContactList contactList) {
        getSelecedObject = contactList;

    }
}


