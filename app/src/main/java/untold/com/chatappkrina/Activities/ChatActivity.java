package untold.com.chatappkrina.Activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import untold.com.chatappkrina.Adapters.ChatTextAdapter;
import untold.com.chatappkrina.ModelClass.Chats;
import untold.com.chatappkrina.R;

public class ChatActivity extends AppCompatActivity {


    Toolbar title_bar;
    Context mContext;
    EditText et_message;
    Button btn_send;
    RecyclerView rv_chat;
    ChatTextAdapter chatTextAdapter;
    private LinearLayoutManager layoutManager;
    List<Chats> chat_details = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mContext = this;
        title_bar = (Toolbar) findViewById(R.id.titlebar);
        title_bar.setTitle("Chats");

        rv_chat = (RecyclerView) findViewById(R.id.rv_messages);
        btn_send  = (Button) findViewById(R.id.btn_sendMsg);
        et_message  = (EditText) findViewById(R.id.et_typeMessage);


        layoutManager = new LinearLayoutManager(mContext);
        rv_chat.setLayoutManager(layoutManager);
        chatTextAdapter = new ChatTextAdapter(chat_details,mContext);
        rv_chat.setAdapter(chatTextAdapter);

        }
}
