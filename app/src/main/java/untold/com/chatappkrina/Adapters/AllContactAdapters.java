package untold.com.chatappkrina.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import untold.com.chatappkrina.Activities.ContactListActivity;
import untold.com.chatappkrina.Activities.setChatContextActivity;
import untold.com.chatappkrina.ModelClass.ContactList;
import untold.com.chatappkrina.R;

public class AllContactAdapters extends RecyclerView.Adapter<AllContactAdapters.ContactViewHolder> {


    private List<ContactList> contactList;
    private Context mContext;
    ContactList contact;

    private List<ContactList> contactLists= new ArrayList<>();

    public AllContactAdapters(List<ContactList> contactVOList, Context mContext) {

        this.contactList = contactVOList;
        this.mContext = mContext;
//        this.contactList = new ArrayList<>();
        this.contactLists.addAll(contactVOList);
    }

    @NonNull
    @Override
    public AllContactAdapters.ContactViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.single_contact_view, null);
        ContactViewHolder contactViewHolder = new ContactViewHolder(view);
        return contactViewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull final AllContactAdapters.ContactViewHolder contactViewHolder, final int i) {

        contact = contactList.get(i);
        //contactViewHolder.ivContactImage.setImageBitmap(contact.getContactImage());
        contactViewHolder.tvContactName.setText(contact.getContactName());
//        contactViewHolder.tvPhoneNumber.setText(contact.getContactNumber());
        final String user = contact.getContactNumber();
        contactViewHolder.ll_contactView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // contactViewHolder.iv_selectContact.setImageResource(R.drawable.baseline_done_white_24);

                ((ContactListActivity)mContext).contactClick(contactLists.get(i));



              //  Intent i = new Intent(mContext,setChatContextActivity.class);


              /*  DatabaseReference mDatabase;
                mDatabase = FirebaseDatabase.getInstance().getReference();
//                Intent i = new Intent(mContext,setChatContextActivity.class);
//                mContext.startActivity(i);
                ColorMap colorMap = new ColorMap();

                for (int j = 0; j < UserDetails.allUsers.size(); j++) {
                    if (UserDetails.allUsers.get(j).getCountact_number().equals(user.replaceAll(" ", ""))) {
                        ArrayList<RequestDetails> requestDetailsArrayList = new ArrayList<RequestDetails>(UserDetails.allUsers.get(j).getRequests().values());
                        for (int k = 0; k < requestDetailsArrayList.size(); k++) {
                            if (colorMap.getColorMap().get(requestDetailsArrayList.get(i).getFrom_user_name()) != null) {
                                colorMap.getColorMap().remove(requestDetailsArrayList.get(i).getFrom_user_name());
                            }
                        }
                        final int random = new Random().nextInt(colorMap.getColorMap().size());
                        final String FromUserName = (new ArrayList<>(colorMap.getColorMap().keySet())).get(random);

                        RequestDetails requestDetails = new RequestDetails();
                        requestDetails.setDatetime(System.currentTimeMillis() + "");
                        requestDetails.setFrom_user_color(colorMap.getColorMap().get(FromUserName));
                        requestDetails.setFrom_user_id(UserDetails.userData.getId());
                        requestDetails.setId(UserDetails.userData.getId());
                        requestDetails.setFrom_user_name(FromUserName);
                        requestDetails.setSubject("hiii"); //TODO Show subject input for this
                        requestDetails.setTo_user_color("");
                        requestDetails.setTo_user_id(UserDetails.allUsers.get(j).getId());
                        requestDetails.setTo_user_name(contact.getContactName());
                        mDatabase.child("Users").child(UserDetails.allUsers.get(j).getId())
                                .child("requests").child(UserDetails.userData.getId()).setValue(requestDetails)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(mContext, "Request sent success", Toast.LENGTH_LONG).show();
                                        // Write was successful!
                                        // ...
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(mContext, "Request sent failed", Toast.LENGTH_LONG).show();
                                        // Write failed
                                        // ...
                                    }
                                });
                        return;
                    }
                }

                sendRequestViaMessage(user);*/

            }
        });
    }




   /* private void sendRequestViaMessage(String number) {

        number = contact.getContactNumber();
        number = number.replace(" ", "");

      new MessageSentAsyncTask(number).execute();




    }*/

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        ImageView ivContactImage;

        TextView tvContactName;
        TextView tvPhoneNumber;
        ImageView iv_selectContact;
        TextView tvRequest;
        RelativeLayout ll_contactView;


        public ContactViewHolder(View itemView) {
            super(itemView);


            tvContactName = (TextView) itemView.findViewById(R.id.tvContactName);
            //iv_request = (ImageButton) itemView.findViewById(R.id.img_Request);
            ll_contactView = (RelativeLayout) itemView.findViewById(R.id.ll_contactView);

            iv_selectContact = (ImageView) itemView.findViewById(R.id.iv_selectContact);

           /* tvPhoneNumber = (TextView) itemView.findViewById(R.id.tvPhoneNumber);
            tvRequest = (TextView)  itemView.findViewById(R.id.tvRequestChat);
*/

        }


    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        contactList.clear();
        if (charText.length() == 0) {
            contactList.addAll(contactLists);
        } else {
            for (ContactList cl : contactLists) {
                if (cl.getContactName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    contactList.add(cl);

                }
            }
        }
        notifyDataSetChanged();
    }


    /*// sent message
     class MessageSentAsyncTask extends AsyncTask<String,String,String> {

        private String finalNumber;

        MessageSentAsyncTask(String number)
        {
           this.finalNumber = number;
        }
         @Override
         protected String doInBackground(String... strings) {


             //Your authentication key
             String authkey = "252372AOfpZSMEB5c18a8f7";
//Multiple mobiles numbers separated by comma

             //String mobiles =  number;


             String codeNumber;
             String codePlusSign = String.valueOf(finalNumber.charAt(0));

// canada code 1
             if(finalNumber.length() == 12) {
                 codeNumber = finalNumber.substring(1, 2);

             }
             else
             {
                 india code
                 codeNumber = finalNumber.substring(1, 2) + finalNumber.substring(2, 3);

             }


             String phoneNumber = finalNumber.substring(finalNumber.length() - 10);
             String countryCode = codePlusSign + codeNumber;

//Sender ID,While using route4 sender id should be 6 characters long.
             //String senderId = "102234";
//Your message to send, Add URL encoding here.
             String message = "Someone from your contact wants to discuss some important thing privately via Untold App Download it from";
//define route
             String route="1";

             URLConnection myURLConnection=null;
             URL myURL=null;
             BufferedReader reader=null;

//encoding message
             String encoded_message=URLEncoder.encode(message);

//Send SMS API
             String mainUrl="http://api.msg91.com/api/sendhttp.php?";


//Prepare parameter string
             StringBuilder sbPostData= new StringBuilder(mainUrl);
             sbPostData.append("country="+codeNumber);
             sbPostData.append("&sender="+"Untold");
             sbPostData.append("&route="+route);
             sbPostData.append("&mobiles="+phoneNumber);
             sbPostData.append("&authkey="+authkey);
             sbPostData.append("&message="+encoded_message);

             // sbPostData.append("&sender="+senderId);

//final string
             mainUrl = sbPostData.toString();
             try
             {
                 //prepare connection
                 myURL = new URL(mainUrl);
                 try {
                     myURLConnection = myURL.openConnection();
                 } catch (IOException e) {
                     e.printStackTrace();
                 }
                 myURLConnection.connect();
                 reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));

                 //reading response
                 String response;
                 while ((response = reader.readLine()) != null)
                     //print response
                     Log.d("RESPONSE", ""+response);
                 //finally close connection
                 reader.close();
             }
             catch (IOException e)
             {
                 e.printStackTrace();
             }

             return null;
         }


         @Override
         protected void onPostExecute(String s) {
             super.onPostExecute(s);

             Toast.makeText(mContext,"Message Sent",Toast.LENGTH_LONG);
         }
     };*/



}
