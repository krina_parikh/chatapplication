package untold.com.chatappkrina.ModelClass;

import java.util.LinkedHashMap;

// Color hasmap
public class ColorMap {


    public  LinkedHashMap<String, String> getColorMap() {
        return meMap;
    }
    LinkedHashMap<String, String> meMap=new LinkedHashMap<String, String>();
    public ColorMap() {

        meMap.put("Color1","Red");
        meMap.put("Color2","Blue");
        meMap.put("Color3","Green");
        meMap.put("Color4","White");
        meMap.put("GRAY","#808080");
        meMap.put("BLACK","#000000");
        meMap.put("RED","#FF0000");
        meMap.put("MAROON","#800000");
        meMap.put("YELLOW","#FFFF00");
        meMap.put("OLIVE","#808000");
        meMap.put("LIME","#00FF00");
        meMap.put("GREEN","#008000");
        meMap.put("AQUA","#00FFFF");
        meMap.put("TEAL","#008080");
        meMap.put("BLUE","#0000FF");
        meMap.put("NAVY","#000080");
        meMap.put("FUCHSIA","#FF00FF");
        meMap.put("PURPLE","#800080");
        meMap.put("PINK","#FF69B4");
        meMap.put("ORANGE","#FF4500");
        meMap.put("VIOLET","#EE82EE");
        meMap.put("DARKVIOLET","#EE82EE");
        meMap.put("TURQUOISE","#40E0D0");
        meMap.put("DODGERBLUE","#1E90FF");
//        meMap.put("MAROON","#800000");
        meMap.put("BROWN","#A52A2A");




        }



}
