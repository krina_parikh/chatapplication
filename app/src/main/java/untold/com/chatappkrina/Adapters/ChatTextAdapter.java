package untold.com.chatappkrina.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import untold.com.chatappkrina.ModelClass.Chats;
import untold.com.chatappkrina.R;

import static untold.com.chatappkrina.ModelClass.Chats.FROM_FRIEND;
import static untold.com.chatappkrina.ModelClass.Chats.FROM_USER;

public class ChatTextAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {


    private List<Chats>  list_of_messages;
    private Context mContext;
    public ChatTextAdapter(List<Chats> chatList, Context mContext){
        this.list_of_messages = chatList;
        this.mContext = mContext;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view ;

        switch (i)
        {
            case FROM_USER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_message_user, null);
                return new chatFromUserViewHolder(view);

            case FROM_FRIEND:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_message_friend, null);
                return new chatFromFriendViewHolder(view);

        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        Chats object = list_of_messages.get(i);
        if (object != null) {
            switch (object.getItem_type()) {
                case FROM_USER:
                    ((chatFromUserViewHolder) viewHolder).tv_seenStatus.setText("hiiii");
                    ((chatFromUserViewHolder) viewHolder).tv_chatContentFromUser.setText("jiiii");
                    ((chatFromUserViewHolder) viewHolder).tv_chatdateTime.setText("jijiijijij");

                    break;
                case FROM_FRIEND:

                  //  ((chatFromFriendViewHolder) viewHolder).tv_chatdateTime.setText(object.getMessageDetail().getReadTime());
                 //   ((chatFromFriendViewHolder) viewHolder).tv_chatContentFromFriend.setText(object.getMessageDetail().getText());

                    break;
            }
        }

    }

    @Override
    public int getItemCount() {
        return list_of_messages.size();
    }



    public static class chatFromUserViewHolder extends RecyclerView.ViewHolder {

        TextView tv_seenStatus;
        TextView tv_chatdateTime;
        TextView tv_chatContentFromUser;
        public chatFromUserViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_chatdateTime = (TextView) itemView.findViewById(R.id.chat_time);
            tv_seenStatus = (TextView) itemView.findViewById(R.id.tv_chatSeenStatus);
            tv_chatContentFromUser = (TextView) itemView.findViewById(R.id.textContentUser);

        }
    }




    public static class chatFromFriendViewHolder extends RecyclerView.ViewHolder {

        TextView tv_chatdateTime;
        TextView tv_chatContentFromFriend;

        public chatFromFriendViewHolder(View itemView) {
            super(itemView);


            tv_chatdateTime = (TextView) itemView.findViewById(R.id.chat_time);
            tv_chatContentFromFriend = (TextView) itemView.findViewById(R.id.textContentFriend);

        }
    }
}
