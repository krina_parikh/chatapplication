package untold.com.chatappkrina.Adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import untold.com.chatappkrina.Activities.setChatContextActivity;
import untold.com.chatappkrina.Interface.onChatSubjectListener;
import untold.com.chatappkrina.ModelClass.Chats;
import untold.com.chatappkrina.ModelClass.ContactList;
import untold.com.chatappkrina.R;
import untold.com.chatappkrina.Utils.KarlaTextView;

import static android.content.Context.CLIPBOARD_SERVICE;

public class ChatContextAdapter extends  RecyclerView.Adapter<ChatContextAdapter.ChatContextViewHolder>{


    Chats chatSubject;
    private List<String> list_of_chatSubject;
    private Context mContext;
     public ChatContextAdapter(Context mContext,List<String> list_of_chatSubject)
    {
        this.mContext  = mContext;
        this.list_of_chatSubject = list_of_chatSubject;
    }

    @NonNull
    @Override
    public ChatContextAdapter.ChatContextViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.chat_subject_context, null);
        ChatContextAdapter.ChatContextViewHolder chatContextViewHolder = new ChatContextAdapter.ChatContextViewHolder(view);
        return chatContextViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ChatContextAdapter.ChatContextViewHolder chatContextViewHolder, final int i) {


     //final  Chats chats = list_of_chatSubject.get(i);
     chatContextViewHolder.tv_chatSubject.setText(list_of_chatSubject.get(i));

     chatContextViewHolder.tv_chatSubject.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             ((setChatContextActivity)mContext).OnChatSubjectClick(list_of_chatSubject.get(i));
         }
     });
    }

    @Override
    public int getItemCount() {
        return list_of_chatSubject.size();
    }

    public class ChatContextViewHolder extends RecyclerView.ViewHolder {

        KarlaTextView tv_chatSubject;

        private ClipboardManager myClipboard;
        private ClipData myClip;

        public ChatContextViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_chatSubject = (KarlaTextView) itemView.findViewById(R.id.tv_chatSubject);
        }
    }
}
