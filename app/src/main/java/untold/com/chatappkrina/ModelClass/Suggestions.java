package untold.com.chatappkrina.ModelClass;

public class Suggestions {


    String suggestion_text;
    String  suggestion_usage;

    public String getSuggestion_text() {
        return suggestion_text;
    }

    public void setSuggestion_text(String suggestion_text) {
        this.suggestion_text = suggestion_text;
    }

    public String getSuggestion_usage() {
        return suggestion_usage;
    }

    public void setSuggestion_usage(String suggestion_usage) {
        this.suggestion_usage = suggestion_usage;
    }
}
