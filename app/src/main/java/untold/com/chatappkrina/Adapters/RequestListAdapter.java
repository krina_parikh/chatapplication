package untold.com.chatappkrina.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import untold.com.chatappkrina.Activities.ChatActivity;
import untold.com.chatappkrina.ModelClass.Chats;
import untold.com.chatappkrina.ModelClass.ContactList;
import untold.com.chatappkrina.ModelClass.RequestDetails;
import untold.com.chatappkrina.R;
import untold.com.chatappkrina.Utils.Constant;
import untold.com.chatappkrina.Utils.KarlaItalicTextView;
import untold.com.chatappkrina.Utils.KarlaTextView;
import untold.com.chatappkrina.Utils.Util;

public class RequestListAdapter extends RecyclerView.Adapter<RequestListAdapter.RequestViewHolder>{


    private List<RequestDetails>  requestDetailsList;
    private Context mContext;

    public RequestListAdapter(List<RequestDetails> requestDetailsList, Context mContext){
        this.requestDetailsList = requestDetailsList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RequestListAdapter.RequestViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_requestdetail, null);
        RequestListAdapter.RequestViewHolder requestViewHolder = new RequestListAdapter.RequestViewHolder(view);
        return requestViewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RequestListAdapter.RequestViewHolder requestViewHolder, final int i) {

         final RequestDetails requestDetails = requestDetailsList.get(i);
        requestViewHolder.tv_chatContext.setText(requestDetails.getSubject());
        requestViewHolder.tv_requestTime.setText(requestDetails.getDatetime());


        requestViewHolder.btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // requestDetailsList.remove(0);

                // remove from the list and set in the chat
                Toast.makeText(mContext,"Request Accepted",Toast.LENGTH_LONG).show();

            }
        });

        requestViewHolder.btn_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showConfirmationDialog(requestDetails.getFrom_user_id(),
                        requestDetailsList.get(i).getSubject(),
                        requestDetailsList.get(i).getDatetime(),
                        0, i);


                // remove it from database list
                Toast.makeText(mContext,"Request Rejected",Toast.LENGTH_LONG).show();
            }
        });

        }

    private void showConfirmationDialog(String from_user_id, String subject, String datetime, final int i, int i1) {

        try {
            final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle("Are you sure");
            builder.setMessage("you want to reject " + subject + "'s request?");
            builder.setPositiveButton(mContext.getString(R.string.str_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (Util.isOnline(mContext)) {
                       // new ApproveEvenRequets(eventID, userID, requestUserID, approveStatus, i).execute();
                    } else
                        Toast.makeText(mContext, Constant.network_error, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton(mContext.getString(R.string.str_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });


            AlertDialog alertConfirmation = builder.create();
            alertConfirmation.show();

            Button nbutton = alertConfirmation.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

            Button pbutton = alertConfirmation.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

        } catch (Exception ex) {
            Log.e("", ex + "");
        }


    }

    @Override
    public int getItemCount() {
        return requestDetailsList.size();
    }


    public static class RequestViewHolder extends RecyclerView.ViewHolder{

        CardView cv_request;
        KarlaTextView tv_chatContext;
        KarlaItalicTextView tv_requestTime;
        Button btn_accept , btn_reject;


        public RequestViewHolder(View itemView) {
            super(itemView);

            tv_chatContext = (KarlaTextView) itemView.findViewById(R.id.tv_chatContext);
            tv_requestTime = (KarlaItalicTextView) itemView.findViewById(R.id.tv_requestTime);
            cv_request = (CardView) itemView.findViewById(R.id.cv_requests);

            btn_accept = (Button) itemView.findViewById(R.id.btn_accept);
            btn_reject = (Button) itemView.findViewById(R.id.btn_reject);


        }
    }


}
